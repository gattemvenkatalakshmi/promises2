const fs = require('fs');
const boardsPath="../boards_1.json"
function boardInformation( boardID){
   // setTimeout(()=>{
        // let boards;
         return new Promise((resolve, reject)=>{
         fs.readFile(boardsPath, "utf-8", (err, data) => {
            if (err) {
              //callback(err, null);
               reject( err);
              return;
            }
            try {
              const board = JSON.parse(data)
    
             const particularboardID=board.filter((board)=> board.id==boardID)
              
              if (particularboardID.length!=0) {
                //callback(null, particularboardID);
                 resolve(particularboardID);
              } else {
                //callback("Board not found", null);
                 reject("not present in board")
              }
            } catch (error) {
              reject("Error parsing JSON");
            }
          });
        });
   // }, 2000)
}
 module.exports = boardInformation;