const fs = require("fs");
const boardJsonPath = "../boards_1.json";
exports.getInformation = function getInformation() {
  return new Promise((resolve, reject) => {
    fs.readFile(boardJsonPath, "utf8", (error, data) => {
      if (error) {
        reject(error);
      } else {
         const boards = JSON.parse(data);
        resolve(boards);
      }
    });
  });
};
exports.getThanosid = function getThanosid(boards) {
  return new Promise((resolve, reject) => {
    try {
      const particularboardID = boards.filter(
        (board) => board.name === "Thanos"
      );
     //console.log(boards)
     resolve(particularboardID[0].id);
    } catch (error) {
      reject(error);
    }
  });
};
 exports.getMindId= function getMindId(lists){
    return new Promise((resolve, reject)=>{
        try {
            
            const particularlistID=lists[0].filter((list)=> list.name =='Mind')
           
           resolve(particularlistID[0].id);
          } catch (error) {
            reject(error);
          }
    })
 }

/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
