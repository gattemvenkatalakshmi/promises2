const boardInformation = require('../callback_1.cjs');
const listInformation = require('../callback_2.cjs');
const cardInformation = require('../callback_3.cjs');
const promise= require('../callback_5.cjs');
promise.getInformation().then( function(data){
    return  promise.getThanosid(data);
  }).then( function(data){
     
   return boardInformation(data);
  }).then(function(data){
    return listInformation(data[0].id);
  }).then(function(data){
      return promise.getMindId(data);
})
.then(async function(data){
     let cardsObj=[];
     for(id of data){
         let ans = await cardInformation(id);
        cardsObj.push(ans[0]);
     }
      return cardsObj;
      
 })
  .then(function( data){
     console.log( data);
  })
   .catch (function(error){
     console.log( error);
  })
 