const fs = require("fs");
const cardsJsonPath = "../cards_1.json";
function cardInformation(cardID, callback) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      fs.readFile(cardsJsonPath, "utf-8", (error, data) => {
        if (error) {
          reject(error);
        }
        try {
          const card = JSON.parse(data);
          const cardArr = Object.entries(card);

          const particularcardID = cardArr
            .map((card) => {
              let obj = {};
              obj[card[0]] = card[1];
              return obj;
            })
            .filter((card) => card[cardID])
            .map((card) => {
              return card[cardID];
            });
          if (particularcardID.length == 0) {
            resolve(particularcardID);
          } else {
            resolve(particularcardID);
          }
        } catch {
          reject("error while parsing");
        }
      });
    }, 2000);
  });
}
module.exports = cardInformation;
