const fs = require("fs");
const boardJsonPath = "../boards_1.json";
exports.getInformation = function getInformation() {
  return new Promise((resolve, reject) => {
    fs.readFile(boardJsonPath, "utf8", (error, data) => {
      if (error) {
        reject(error);
      } else {
         const boards = JSON.parse(data);
        resolve(boards);
      }
    });
  });
};
exports.getThanosid = function getThanosid(boards) {
  return new Promise((resolve, reject) => {
    try {
      const particularboardID = boards.filter(
        (board) => board.name === "Thanos"
      );
     //console.log(boards)
     resolve(particularboardID[0].id);
    } catch (error) {
      reject(error);
    }
  });
};
 exports.getMindId= function getMindId(lists){
    return new Promise((resolve, reject)=>{
        try {
            
            const particularlistID=lists[0].filter((list)=> list.name =='Mind'||list.name=='Space')
            .map((list)=>list.id);
           resolve(particularlistID);
          } catch (error) {
            reject(error);
          }
    })
 }
